<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Contact;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use Redirect;
use Session;

class ContactController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $item = Contact::get()->first();
        return view('admin.contact.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request, $id = 0) {
        try {

            if ($id == 0) {
                $item = new Contact;
            } else {
                $item = Contact::find($id);
            }


            $item->title = $request->input('title');

            $item->contact_desc = $request->input('contact_desc');

            $item->desc = $request->input('desc');

            $item->email = $request->input('email');

            $item->adress_cx = $request->input('adress_cx');

            $item->tel_cx = $request->input('tel_cx');

            $item->adress_fr = $request->input('adress_fr');

            $item->tel_fr = $request->input('tel_fr');

            $item->whatss = $request->input('whatss');

            $item->titleform = $request->input('titleform');


            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/contact')->with('status', 'Post realizado com sucesso!');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

}
