<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Process;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use Redirect;
use Session;

class ProcessController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $item = Process::get()->first();
        return view('admin.process.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request, $id = 0) {
        try {

            if ($id == 0) {
                $item = new Process;
            } else {
                $item = Process::find($id);
            }


            $item->title = $request->input('title');
            
            $item->desc = $request->input('desc');
            
            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/process')->with('status', 'Post realizado com sucesso!');
    }


    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

   

}
