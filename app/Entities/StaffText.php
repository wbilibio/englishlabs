<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class StaffText extends Model
{
    protected $table = 'stafftext';
    protected $fillable = ['title', 'desc'];
}