<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProcessGrafic extends Model
{
    protected $table = 'whatoffergrafic';
    protected $fillable = ['image', 'desc'];
}