<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PeloMundoText extends Model
{
    protected $table = 'pelomundotext';
    protected $fillable = ['title', 'desc'];
}