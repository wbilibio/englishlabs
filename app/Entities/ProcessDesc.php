<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProcessDesc extends Model
{
    protected $table = 'processdesc';
    protected $fillable = ['image', 'desc'];
}