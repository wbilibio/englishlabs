<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = ['image', 'title', 'desc', 'link_face', 'link_insta', 'order'];
}