<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Bannersfr extends Model
{
    protected $table = 'bannersfr';
    protected $fillable = ['image'];
}