<!--footer area start-->


   


<footer class="footer-container">
    <div class="footera-area section-padding wow fadeInDown">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 col-lg-5">
                    <div class="logo-area footer">
                        <a href="#"><img src="<?php echo e(asset('front/images/logoenglishlabs-branco.png')); ?>" alt=""></a>
                    </div>
                    <div class="space-20"></div>
                    <p class="">Escola particular de idiomas. Inglês e espanhol.</p>
                    <div class="space-10"></div>
                    <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos direitos reservados | English Labs</p>
                </div>
                <div class="col-xs-12 col-md-4 col-lg-3">
                    <div class="single-footer justify-content-end">
                        <ul>
                            <li><a class="scroll" href="#home">Home</a></li>
                            <li><a class="scroll" href="#sobre"><?php echo e(!empty($data['about']->title_about) ? $data['about']->title_about : ''); ?></a></li>
                            <li><a class="scroll" href="#nossos-processos"><?php echo e(!empty($data['processtext']->title) ? $data['processtext']->title : ''); ?></a></li>
                            <li><a class="scroll" href="#proficiencia"><?php echo e(!empty($data['proficiencytext']->title) ? $data['proficiencytext']->title : ''); ?></a></li>
                            <li><a class="scroll" href="#pelo-mundo"><?php echo e(!empty($data['pelomundotext']->title) ? $data['pelomundotext']->title : ''); ?></a></li>
                            <li><a class="scroll" href="#staff"><?php echo e(!empty($data['stafftext']->title) ? $data['stafftext']->title : ''); ?></a></li>
                            <li><a class="scroll" href="#depoimentos"><?php echo e(!empty($data['depositionstext']->title) ? $data['depositionstext']->title : ''); ?></a></li>
                            <li><a class="scroll" href="#contato"><?php echo e(!empty($data['contatotext']->title) ? $data['contatotext']->title : ''); ?></a></li>
                        </ul>
                    </div>
                </div> 
                <div class="col-xs-12 col-lg-4 col-md-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-footer-social social-icons sc">
                                <a href="http://facebook.com/enjoyenglishlabs" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="http://instagram.com/englishlabs" target="_blank"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="adress-footer">
                                <p class="text-center text-contat"><b>CAXIAS DO SUL
                                    <?php if(!empty($data['contatotext']->tel_cx)): ?>
                                     -  <?php echo e($data['contatotext']->tel_cx); ?></b></p>
                                    <?php endif; ?>
                                <p class="text-center adress-cx">
                                    <?php if(!empty($data['contatotext']->adress_cx)): ?>
                                         <?php echo e($data['contatotext']->adress_cx); ?>

                                    <?php endif; ?>
                                </p>


                                <p class="text-center text-contat"><b>FARROUPILHA
                                    <?php if(!empty($data['contatotext']->tel_fr)): ?>
                                    - <?php echo e($data['contatotext']->tel_fr); ?></b></p>                          
                                    <?php endif; ?>
                                <p class="text-center"> 
                                    <?php if(!empty($data['contatotext']->adress_fr)): ?>
                                       <?php echo e($data['contatotext']->adress_fr); ?>

                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</footer>    
<!--footer area end-->
<!-- main js-->
<script src="<?php echo e(asset('front/js/jquery-2.2.4.min.js')); ?>"></script>

<script src="<?php echo e(asset('front/js/parallax.min.js')); ?>"></script>
<!--core js-->
<script src="<?php echo e(asset('front/js/core.js')); ?>"></script>
<!--theme js-->
<script src="<?php echo e(asset('front/js/theme.js')); ?>"></script>

<!-- popper js-->
<script src="<?php echo e(asset('front/js/popper.js')); ?>"></script>
<!-- carousel js-->
<script src="<?php echo e(asset('front/js/owl.carousel.min.js')); ?>"></script>
<!-- wow js-->
<script src="<?php echo e(asset('front/js/wow.min.js')); ?>"></script>
<!-- bootstrap js-->
<script src="<?php echo e(asset('front/js/bootstrap.min.js')); ?>"></script>
<!--mobile menu js-->
<script src="<?php echo e(asset('front/js/jquery.slicknav.min.js')); ?>"></script>
<!-- map file should be included after ammap.js -->
<script src="<?php echo e(asset('front/ammap/ammap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('front/ammap/maps/js/worldLow.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('front/ammap/themes/black.js')); ?>" type="text/javascript"></script>

<!-- map file should be included after ammap.js -->
<script src="<?php echo e(asset('front/js/main.js')); ?>"></script>