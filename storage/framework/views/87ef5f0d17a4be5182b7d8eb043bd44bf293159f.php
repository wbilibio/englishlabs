<?php $__env->startSection('title', 'AdminLTE'); ?>

<?php $__env->startSection('content_header'); ?>
<h1>Depoimentos</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">
                <?php if(!empty($items)): ?>
                <h3 class="box-title">Lista Depoimentos</h3>
                <?php else: ?>
                <h3 class="box-title">Nenhum um item cadastrado</h3>
                <?php endif; ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php if(session('status')): ?>
                <div class="alert alert-success">
                    <?php echo e(session('status')); ?>

                </div>
                <?php endif; ?>
                <table id="tabela" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Título</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($item->id); ?></td>
                            <td><img width="80" src="<?php echo e(asset('_files/depoimentos/'.$item->image)); ?>"></td>
                            <td><?php echo e($item->title); ?></td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" class="btn btn-success" href="<?php echo e(route('admin.depositionsdesc.editar.{id}', array($item->id))); ?>" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                    <a type="link"  class="btn btn-danger" href="<?php echo e(route('admin.depositionsdesc.delete.{id}', array($item->id))); ?>" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Título</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Adicionar Banner</h3>
            </div>
            <form action="<?php echo e(route('admin.depositionsdesc.store')); ?>" class="form-table-list" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="image">Imagem:</label>
                                <div class="upload">
                                    <div class="btn btn-primary">Adicionar Imagem</div>
                                    <input type="file" class="input-file" id="image" name="file_image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" placeholder="Descrição" name="title" value="">
                            </div>
                            <div class="form-group">
                                <label>Descrição</label>
                                <input type="text" class="form-control" placeholder="Descrição" name="desc" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success btn-send pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/css/main.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $('#tabela').DataTable({
        language: {
            emptyTable: "Nenhum registro encontrado",
            info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            infoEmpty: "Mostrando 0 até 0 de 0 registros",
            infoFiltered: "(Filtrados de _MAX_ registros)",
            infoPostFix: "",
            infoThousands: ".",
            lengthMenu: "_MENU_ resultados por página",
            loadingRecords: "Carregando...",
            processing: "Processando...",
            zeroRecords: "Nenhum registro encontrado",
            search: "Pesquisar",
            paginate: {
                next: "Próximo",
                previous: "Anterior",
                first: "Primeiro",
                last: "Último"
            },
            aria: {
                sortAscending: ": Ordenar colunas de forma ascendente",
                sortDescending: ": Ordenar colunas de forma descendente"
            }
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>