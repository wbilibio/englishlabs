<?php $__env->startSection('title', 'AdminLTE'); ?>

<?php $__env->startSection('content_header'); ?>
<h1>Sobre nós</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">

                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="<?php echo e(empty($item->id) ? route('admin.about.store') : route('admin.about.store.{id}', array($item->id))); ?>" class="form-table-list" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <!-- text input -->
                            <div class="form-group">
                                <label>Título quem somos</label>
                                <input type="text" class="form-control" placeholder="Título" name="title_about" value="<?php echo e(empty($item->title_about) ? old('title_about') : $item->title_about); ?>">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto quem somos</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="text_about"><?php echo e(empty($item->text_about) ? old('text_about') : $item->text_about); ?></textarea>
                            </div>
                            
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto Caxias do Sul</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="textcx"><?php echo e(empty($item->textcx) ? old('textcx') : $item->textcx); ?></textarea>
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto Farroupilha</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="textfr"><?php echo e(empty($item->textfr) ? old('textfr') : $item->textfr); ?></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
<?php echo $__env->yieldContent('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>