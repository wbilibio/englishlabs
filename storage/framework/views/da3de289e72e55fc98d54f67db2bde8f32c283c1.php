<?php $__env->startSection('title', 'AdminLTE'); ?>

<?php $__env->startSection('content_header'); ?>
<h1>Staff</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">
                <?php if(!empty($items)): ?>
                <h3 class="box-title">Lista Staff</h3>
                <?php else: ?>
                <h3 class="box-title">Nenhum um item cadastrado</h3>
                <?php endif; ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php if(session('status')): ?>
                <div class="alert alert-success">
                    <?php echo e(session('status')); ?>

                </div>
                <?php endif; ?>
                <div class="col-md-12 color-palette">
                    <table id="tabela" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th>Imagem</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-url="<?php echo e(route('admin.staff.reorder')); ?>">
                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="ui-state-default" data-id="<?php echo e($item->id); ?>">
                                <td><?php echo e($item->id); ?></td>
                                <td><?php echo e($item->title); ?></td>
                                <td><img width="80" src="<?php echo e(asset('_files/staff/'.$item->image)); ?>"></td>
                                <td>
                                    <div class="btn-group">
                                        <a type="button" class="btn btn-success" href="<?php echo e(route('admin.staff.editar.{id}', array($item->id))); ?>" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                        <a type="link"  class="btn btn-danger" href="<?php echo e(route('admin.staff.delete.{id}', array($item->id))); ?>" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>


                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Adicionar Banner</h3>
            </div>
            <form action="<?php echo e(route('admin.staff.store')); ?>" class="form-table-list" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="image">Imagem:</label>
                                <div class="upload">
                                    <div class="btn btn-primary">Adicionar Imagem</div>
                                    <input type="file" class="input-file" id="image" name="file_image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" placeholder="Nome" name="title" value="<?php echo e(empty($item->title) ? old('title') : $item->title); ?>">
                            </div>
                            <div class="form-group">
                                <label>Descrição</label>
                                <input type="text" class="form-control" placeholder="Descrição" name="desc" value="<?php echo e(empty($item->desc) ? old('desc') : $item->desc); ?>">
                            </div>
                            <div class="form-group">
                                <label>Facebook</label>
                                <input type="text" class="form-control" placeholder="Facebook" name="link_face" value="<?php echo e(empty($item->link_face) ? old('link_face') : $item->link_face); ?>">
                            </div>
                            <div class="form-group">
                                <label>Instagram</label>
                                <input type="text" class="form-control" placeholder="Instagram" name="link_insta" value="<?php echo e(empty($item->link_insta) ? old('link_insta') : $item->link_insta); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success btn-send pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/css/main.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('js/jquery-ui.js')); ?>"></script>
<script>

    var App = App || {};
    App.sortableGrid = function () {
        $('.sortable').each(function (idx, el) {
            var sortable = $(el);
            var obj = {};
            var url = sortable.attr('data-url');
            sortable.sortable().on('sortupdate', function (event, ui) {
                var items = [];
                sortable.find('.ui-state-default').each(function (i) {
                    var order = sortable.find('.ui-state-default').length - i;
                    obj = {'id': $(this).data('id'), 'order': order};
                    items.push(obj);
                });
                App.reorder(items, url);
            });

            sortable.disableSelection();
        });
    };

    App.reorder = function (items, url) {
        if (items.length > 0) {
            $.ajax({
                url: url,
                headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')},
                type: "POST",
                data: {items: items},
                success: function (data) {
                    if (data == 1)
                        Admin.msgSuccessGallery = true;
                    else if (data == 2)
                        Admin.msgSuccessReorder = true;
                    Admin.msgSuccess = 'Item reordenado com sucesso!';
                }
            });
        }
    };
    App.sortableGrid();
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>