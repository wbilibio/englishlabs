<?php $__env->startSection('title', 'AdminLTE'); ?>


<?php $__env->startSection('content_header'); ?>
<h1>Contato</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">

                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="<?php echo e(empty($item->id) ? route('admin.contact.store') : route('admin.contact.store.{id}', array($item->id))); ?>" class="form-table-list" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <!-- text input -->
                            <div class="form-group">
                                <label>Título contato</label>
                                <input type="text" class="form-control" placeholder="Título" name="title" value="<?php echo e(empty($item->title) ? old('title') : $item->title); ?>">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição contato</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="contact_desc"><?php echo e(empty($item->contact_desc) ? old('contact_desc') : $item->contact_desc); ?></textarea>
                            </div>
                            <div class="form-group" >
                                <label>Título Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="titledesc"><?php echo e(empty($item->titledesc) ? old('titledesc') : $item->titledesc); ?></textarea>
                            </div>
                              <div class="form-group" >
                                <label>Título Formulario</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="titleform"><?php echo e(empty($item->titleform) ? old('titleform') : $item->titleform); ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" placeholder="Título" name="email" value="<?php echo e(empty($item->email) ? old('email') : $item->email); ?>">
                            </div>

                             <div class="form-group">
                                <label>whatss</label>
                                <input type="text" class="form-control" placeholder="WhatsApp" name="whatss" value="<?php echo e(empty($item->whatss) ? old('whatss') : $item->whatss); ?>">
                            </div>
                            <div class="form-group">
                                <label>Endereço Caxias</label>
                                <input type="text" class="form-control" placeholder="Título" name="adress_cx" value="<?php echo e(empty($item->adress_cx) ? old('adress_cx') : $item->adress_cx); ?>">
                            </div>
                            <div class="form-group">
                                <label>Telefone Caxias</label>
                                <input type="text" class="form-control" placeholder="Título" name="tel_cx" value="<?php echo e(empty($item->tel_cx) ? old('tel_cx') : $item->tel_cx); ?>">
                            </div>
                            <div class="form-group">
                                <label>Endereço Farroupilha</label>
                                <input type="text" class="form-control" placeholder="Título" name="adress_fr" value="<?php echo e(empty($item->adress_fr) ? old('adress_fr') : $item->adress_fr); ?>">
                            </div>
                            <div class="form-group">
                                <label>Telefone Farroupilha</label>
                                <input type="text" class="form-control" placeholder="Título" name="tel_fr" value="<?php echo e(empty($item->tel_fr) ? old('tel_fr') : $item->tel_fr); ?>">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
<?php echo $__env->yieldContent('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>