<?php $__env->startSection('content'); ?>
     <section class="welcome-area wow fadeInUp" id="home">
        <!-- Main Content -->
            <!-- Scroll Down Line -->
            <a href="#sobre" class="scroll-down scroll">
                <div class="container">
                  <div class="chevron"></div>
                  <div class="chevron"></div>
                  <div class="chevron"></div>
                </div>
            </a>
            <!-- Hover List -->
            <div class="hover-list py-12 px-6">

                <!-- Hover List MEDIA -->
                <div class="hover-list-media swiper" data-effect="fade">
                    <?php $__currentLoopData = $data['banners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($banner)): ?>
                            <!-- Media Item -->
                            <div class="swiper-slide">
                                <div class="slide-media" data-poster="<?php echo e(asset('_files/banners/'.$banner->image)); ?>"></div>
                            </div>
                            <!-- /Media Item -->
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- /Hover List MEDIA -->

                <!-- Hover List TITLES -->
                <div class="container text-light text-center px-0 ">
                    <div class="hover-list-links hover-list-grid row no-gutters">

                        <!-- Title Item -->
                        <div class="item col-lg-6">
                            <a href="#sobre-caxias">
                                <h1>Unidade</h1>
                                <h5 class="item-title">Caxias do Sul</h5>
                            </a>
                        </div>
                        <!-- /Title Item -->

                        <!-- Title Item -->
                        <div class="item col-lg-6">
                            <a href="#sobre-farroupilha">
                                <h1>Unidade</h1>
                                <h5 class="item-title">Farroupilha</h5>
                            </a>
                        </div>
                        <!-- /Title Item -->

                    </div>
                </div>
                <!-- /Hover List TITLES -->

            </div>
            <!-- /Hover List -->
     </section>

    <section class="section-full parallax-window" id="sobre" data-z-index="2" data-parallax="scroll" data-image-src="<?php echo e(asset('front/images/bg-quem-somos.png')); ?>" data-natural-width="1200" data-natural-height="800">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-4 col-xs-12 text">
                    <h1>QUEM SOMOS</h1>
                    <?php if(!empty($data['about'])): ?>
                        <div class="heading-white">
                            <p>
                                <?php echo $data['about']->text_about; ?>

                            </p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-1 justify-content-end"></div>
            </div>
        </div>
    </section>

    <!--Documentation area start-->
    <section class="nossos-processos wow" id="nossos-processos">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading heading-purple">
                        <?php if(!empty($data['processtext']->title)): ?>
                            <h1><?php echo e($data['processtext']->title); ?></h1>
                        <?php endif; ?>
                        <div class="space-10"></div>
                        <?php if(!empty($data['processtext']->desc)): ?>
                            <p class="white"><?php echo $data['processtext']->desc; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php if(!empty($data['processitems'])): ?>
                    <?php $__currentLoopData = $data['processitems']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $processitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <div class="col-12 col-md-6 col-lg-2">
                          <div class="single-document">
                            <div class="document-flag text-center">
                                <img class="img-desc justify-content-center" src="<?php echo e(asset('_files/processos/'.$processitem->image)); ?>" alt="">
                            </div>
                          </div>
                          <?php if(!empty($processitem->desc)): ?>
                              <div class="descricao">
                                <p><?php echo $processitem->desc; ?></p>
                              </div>
                          <?php endif; ?>
                       </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            <div class="row justify-content-center">
                <?php if(!empty($data['processgrafic'])): ?>
                    <?php $__currentLoopData = $data['processgrafic']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $processgrafic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <div class="col-12">
                            <img class="item-grafic justify-content-center" src="<?php echo e(asset('_files/processos/'.$processgrafic->image)); ?>" alt="">
                       </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
       </div>
    </section>
    <section class="proficiencia wow" id="proficiencia">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading">
                        <?php if(!empty($data['proficiencytext']->title)): ?>
                            <h1><?php echo e($data['proficiencytext']->title); ?></h1>
                        <?php endif; ?>
                        <div class="space-10"></div>
                        <?php if(!empty($data['proficiencytext']->desc)): ?>
                            <p class="white"><?php echo $data['proficiencytext']->desc; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>

            <div class="row justify-content-center">
                <?php if(!empty($data['proficiencyitems'])): ?>
                    <?php $__currentLoopData = $data['proficiencyitems']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proficiencyitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(!empty($proficiencyitem->popup)): ?>
                    <div class="popup"><p><?php echo $proficiencyitem->popup; ?></p></div>
                    <?php endif; ?>
                       <div class="col-12 col-md-12 col-lg-2">
                          <div class="single-document">
<<<<<<< HEAD
                            <div class="document-flag text-center">
                                <img class="img-desc justify-content-center" src="<?php echo e(asset('_files/proficiencia/'.$proficiencyitem->image)); ?>" alt="">
                            </div>
=======
                            <?php if(!empty($proficiencyitem->popup)): ?>
                                <div class="pop-text">
                                    <div class="closed">X</div>
                                    <div class="text-popup">
                                        <?php echo $proficiencyitem->popup; ?>

                                    </div>
                                </div>
                                <a href="" class="btn-open-modal">
                                    <div class="document-flag text-center">
                                        <img class="img-desc justify-content-center" src="<?php echo e(asset('_files/proficiencia/'.$proficiencyitem->image)); ?>" alt="">
                                    </div>
                                </a>
                            <?php endif; ?>
>>>>>>> 79bfab97ee1f111b4fcf93dcf055d0e1e4f57d19
                          </div>
                          <?php if(!empty($proficiencyitem->desc)): ?>
                              <div class="descricao">
                                <p><?php echo $proficiencyitem->desc; ?></p>
                              </div>
                          <?php endif; ?>
                       </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>

            <div class="row call">
                <div class="space-10"></div>
                <?php if(!empty($data['proficiencytext']->call)): ?>
                    <p class="white"><?php echo $data['proficiencytext']->call; ?></p>
                <?php endif; ?>
            </div>
            <div class="space-60"></div>
        </div>
    </section>
    <section class="pelo-mundo" id="pelo-mundo">
        <div class="container">
             <div class="row">
                <div class="col-12 text-center">
                    <div class="heading heading-purple">
                        <?php if(!empty($data['pelomundotext']->title)): ?>
                            <h1 class="purple"><?php echo e($data['pelomundotext']->title); ?></h1>
                        <?php endif; ?>
                        <div class="space-10"></div>
                        <?php if(!empty($data['pelomundotext']->desc)): ?>
                            <p class="purple"><?php echo $data['pelomundotext']->desc; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="space-60"></div>
                </div>
             </div>
        </div>
         <div class="map-container">
            <div id="mapdiv" class="mapdiv"></div>
         </div>
    </section>
    <section class="section-staff" id="staff">
        <div class="team-area wow fadeInUp section-padding" id="team">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            <?php if(!empty($data['stafftext']->title)): ?>
                                <h1><?php echo e($data['stafftext']->title); ?></h1>
                            <?php endif; ?>
                            <div class="space-10"></div>
                            <?php if(!empty($data['stafftext']->desc)): ?>
                                <p class="white"><?php echo $data['stafftext']->desc; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
                <div class="row text-center">
                    <?php if(!empty($data['staffs'])): ?>
                        <?php $__currentLoopData = $data['staffs']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-12 col-md-4 col-lg-3 item-staff">
                                <div class="single-team">
                                    <div class="single-team-img">
                                        <img src="<?php echo e(asset('_files/staff/'.$staff->image)); ?>" alt="">
                                    </div>
                                    <div class="space-30"></div>
                                    <div class="single-team-content">
                                        <?php if(!empty($staff->title)): ?>
                                            <h3><?php echo e($staff->title); ?></h3>
                                        <?php endif; ?>
                                        <div class="space-10"></div>
                                        <?php if(!empty($staff->desc)): ?>
                                            <h6><?php echo e($staff->desc); ?></h6>
                                        <?php endif; ?>
                                    </div>
                                    <div class="space-10"></div>
                                    <div class="single-team-social">
                                        <ul>
                                            <?php if(!empty($staff->link_face)): ?>
                                                <li><a href="<?php echo e($staff->link_face); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                            <?php endif; ?>
                                            <?php if(!empty($staff->link_insta)): ?>
                                                <li><a href="<?php echo e($staff->link_insta); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <section class="" id="sobre-caxias">
        <div class="scroll-down scroll-right">
          <div class="chevron"></div>
          <div class="chevron"></div>
          <div class="chevron"></div>
        </div>
        <!-- Horizontal Slider -->
        <div class="swiper hero-fullscreen lightbox" data-slides-per-view="auto" data-mousewheel="false" data-keyboard="true">
            <figure class="swiper-slide bg-light vw-50 d-flex align-items-center">
                <figcaption class="p-7">
                    <h1>UNIDADE</h1>
                    <h3 class="bold uppercase align-items-center">CAXIAS DO SUL</h3>
                     <?php if(!empty($data['about'])): ?>
                        <p>
                            <?php echo $data['about']->textcx; ?>

                        </p>
                    <?php endif; ?>
                </figcaption>
            </figure>
            <?php if(!empty($data['bannerscx'])): ?>
                <?php $__currentLoopData = $data['bannerscx']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bannercx): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <figure class="swiper-slide portfolio-item vw-50" data-background="<?php echo e(asset('_files/bannerscx/'.$bannercx->image)); ?>">
                        <a href="<?php echo e(asset('_files/bannerscx/'.$bannercx->image)); ?>" class="lightbox-link" title=""></a>
                    </figure>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </div>
        <!-- /Horizontal Slider -->
    </section>

    <section class="" id="sobre-farroupilha">
        <!-- Scroll Down Line -->
        <div class="scroll-down scroll-left">
          <div class="chevron"></div>
          <div class="chevron"></div>
          <div class="chevron"></div>
        </div>
        <!-- Horizontal Slider -->
        <div class="swiper hero-fullscreen lightbox" data-slides-per-view="auto" data-initial-slide="9" data-mousewheel="false" data-keyboard="true">
            <?php if(!empty($data['bannersfr'])): ?>
                <?php $__currentLoopData = $data['bannersfr']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bannerfr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <figure class="swiper-slide portfolio-item vw-50" data-background="<?php echo e(asset('_files/bannersfr/'.$bannerfr->image)); ?>">
                        <a href="<?php echo e(asset('_files/bannersfr/'.$bannerfr->image)); ?>" class="lightbox-link" title=""></a>
                    </figure>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
             <figure class="swiper-slide bg-gallery-2 vw-50 d-flex align-items-center">
                <figcaption class="p-7">
                    <h1 class="bold uppercase align-items-center">UNIDADE</h1>
                    <h3 class="bold uppercase white">FARROUPILHA</h3>
                    <?php if(!empty($data['about'])): ?>
                        <div class="white">
                            <?php echo $data['about']->textfr; ?>

                        </div>
                    <?php endif; ?>
                </figcaption>
            </figure>
        </div>
        <!-- /Horizontal Slider -->
    </section>
    <section class="bg-degrade">
        <section id="depoimentos" class="depoimentos">
             <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            <?php if(!empty($data['depositionstext']->title)): ?>
                                <h1 class="white"><?php echo e($data['depositionstext']->title); ?></h1>
                            <?php endif; ?>
                            <div class="space-10"></div>
                            <?php if(!empty($data['depositionstext']->desc)): ?>
                                <div class="white"><?php echo $data['depositionstext']->desc; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
                <div class="main">
                    <div class="scroll-down scroll-right">
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                    </div>
                    <div class="scroll-down scroll-left">
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                        <div class="chevron"></div>
                    </div>
                    <div class="hero split-slider">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="swiper bg-lines" data-swiper-name="captions" data-sync="images" data-mousewheel="false" data-keyboard="true" data-autoplay="4000" data-pagination="true" data-loop="true">
                                    <?php if(!empty($data['depositionsdesc'])): ?>
                                        <?php $__currentLoopData = $data['depositionsdesc']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deposition): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="swiper-slide d-flex align-items-center justify-content-center text-center h-100 px-8">
                                                <div class="inner">
                                                    <?php if(!empty($deposition->title)): ?>
                                                        <h1 class="bold uppercase"><?php echo e($deposition->title); ?></h1>
                                                    <?php endif; ?>
                                                    <?php if(!empty($deposition->desc)): ?>
                                                        <p><?php echo $deposition->desc; ?></p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- Images Slider -->
                                <div class="swiper" data-sync="captions" data-swiper-name="images" data-mousewheel="false" data-effect="fade" data-loop="true" >
                                    <?php if(!empty($data['depositionsdesc'])): ?>
                                        <?php $__currentLoopData = $data['depositionsdesc']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deposition): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="swiper-slide" data-background="<?php echo e(asset('_files/depoimentos/'.$deposition->image)); ?>"></div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                                <!-- /Images Slider -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>






        <section id="eventos" class="eventos">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            <?php if(!empty($data['eventtext']->title)): ?>
                                <h1><?php echo e($data['eventtext']->title); ?></h1>
                            <?php endif; ?>
                            <div class="space-10"></div>
                            <?php if(!empty($data['eventtext']->desc)): ?>
                                <p class="white"><?php echo $data['eventtext']->desc; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
            
            <div class="scroll-down scroll-right" >
                <div class="chevron"></div>
                <div class="chevron"></div>
                <div class="chevron"></div>
            </div>
            <div class="scroll-down scroll-left">
                <div class="chevron"></div>
                <div class="chevron"></div>
                <div class="chevron"></div>
            </div>
             <div class="swiper-container" data-mousewheel="false" data-keyboard="true" data-autoplay="4000" data-pagination="true" data-loop="true">
                <div class="swiper-wrapper">
                    <?php if(!empty($data['events'])): ?>
                        <?php $__currentLoopData = $data['events']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<<<<<<< HEAD
                            <div class="col-md-6">
                                <figure class="parallax-folio-item" id="0">
                                <div class="item-media background-parallax" data-background="<?php echo e(asset('_files/eventos/'.$event->image)); ?>"></div>
                                    <figcaption>
                                        <div class="item-caption-inner">
                                            <p class="text-links">
                                                <a href="#"><?php echo e($event->data); ?></a>
                                            </p>
                                            <h3 class="bold uppercase"><?php echo e($event->desc); ?></h3>
                                            <p>
                                                <?php echo $event->text; ?>

                                            </p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
=======
                        
                            <div class="swiper-slide parallax-folio-item" >
                                        
                                            <div class=" item-media background-parallax" data-background="<?php echo e(asset('_files/eventos/'.$event->image)); ?>"></div>
                                                <figcaption>
                                                    <div class="item-caption-inner">
                                                        <p class="text-links">
                                                            <a href="#"><?php echo e($event->data); ?></a>
                                                        </p>
                                                        <h3 class="bold uppercase"><?php echo e($event->desc); ?></h3>
                                                        <p>
                                                            <?php echo $event->text; ?>

                                                        </p>
                                                    </div>
                                                </figcaption>
                                        </figure>
                                    </div>
                                
>>>>>>> 1bf5bdea572820c87d042fea3dc8e70778a78989
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php endif; ?>
                     </div>
            </div>
            </div>
        </section>
    </section>
    <section id="contato" class="contato">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading">
                        <?php if(!empty($data['contatotext']->title)): ?>
                            <h1><?php echo e($data['contatotext']->title); ?></h1>
                        <?php endif; ?>
                        <div class="space-10"></div>
                        <?php if(!empty($data['contatotext']->contact_desc)): ?>
                            <div><?php echo $data['contatotext']->contact_desc; ?></div>
                        <?php endif; ?>
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?php if(!empty($data['contatotext']->titledesc)): ?>
                        <h5 class="my-5"><?php echo $data['contatotext']->titledesc; ?></h5>
                    <?php endif; ?>
                     <p class="text-contat"><b>CAXIAS DO SUL
                                <?php if(!empty($data['contatotext']->tel_cx)): ?>
                                 -  <?php echo e($data['contatotext']->tel_cx); ?></b></p>
                                <?php endif; ?>
                            <p>
                                <?php if(!empty($data['contatotext']->adress_cx)): ?>
                                     <?php echo e($data['contatotext']->adress_cx); ?>

                                <?php endif; ?>
                            </p>
                            <p class="text-contat"><b>FARROUPILHA
                                <?php if(!empty($data['contatotext']->tel_fr)): ?>
                                - <?php echo e($data['contatotext']->tel_fr); ?></b></p>                          
                                <?php endif; ?>
                            <p> 
                                <?php if(!empty($data['contatotext']->adress_fr)): ?>
                                   <?php echo e($data['contatotext']->adress_fr); ?>

                                <?php endif; ?>
                            </p>
                    

                    <?php if(!empty($data['contatotext']->whatss)): ?>
                        <p><b>WhatsApp</b> <?php echo e($data['contatotext']->whatss); ?></p>
                    <?php endif; ?>
                    
                    <?php if(!empty($data['contatotext']->email)): ?>
                        <p><b>EMAIL:</b> <?php echo e($data['contatotext']->email); ?></p>
                    <?php endif; ?>
                    
                </div>
                <div class="col-md-7">
                    <div class="mail-form-message d-none mb-5"></div>
                        <div class="box-header with-border">
                          <?php if(Session::has('success')): ?>
                                    <div class="send-success" role="alert"><?php echo Session::get('success'); ?></div>
                                <?php endif; ?>
                                <?php if(Session::has('error')): ?>
                                    <div class="send-error" role="alert"><?php echo Session::get('error'); ?></div>
                                <?php endif; ?>
                                <?php if(count($errors) > 0): ?>
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                <?php endif; ?>
                        </div>
                    <form class="mail-form" method="POST" action="<?php echo e(route('send')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-12 ">
                                  <?php if(!empty($data['contatotext']->titleform)): ?>
                                <h3 class="uppercase center-block"><?php echo $data['contatotext']->titleform; ?></h3>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-6">
                                <input name="name" type="text" placeholder="Nome:" required>
                            </div>
                            <div class="col-md-6">
                                <input name="phone" type="text" placeholder="Telefone:" required>
                            </div>
                            <div class="col-md-12">
                                <input name="email" type="email" placeholder="Email:" required>
                                <textarea name="text" placeholder="Mensagem:" required rows="4"></textarea>
                                <button type="submit" class="button style-5 uppercase button-contato">Enviar</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mp-maps">
                <!-- Google Map -->
                <div class="googlemaps">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <p>FAÇA UM TOUR 360º PELA NOSSA CASINHA EM CAXIAS DO SUL</p>
                        </div>
                        <div class="col-md-6">
                            <p></p>
                        </div>
                        <div class="col-md-6 pad-maps">
                             <iframe src="https://www.google.com/maps/embed?pb=!4v1537293161997!6m8!1m7!1sCAoSLEFGMVFpcE10NHhfOTc1WG9fRmN2Q3hkZTBqcm5ISmtMdnJIUE5xM1FoeDNq!2m2!1d-29.170614566356!2d-51.18602981209!3f248.1120708525629!4f1.2449362208804047!5f0.7820865974627469" height="450" frameborder="0" style="border:0;width:100%;" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-6 pad-maps">
                            <div id="map" style="height: 450px;"></div>
                        </div>
                        <script src="http://maps.google.com/maps/api/js?key=AIzaSyCtwWwyEUBEqnW0dXhl3tGlqOje5YwCtHA&sensor=false" type="text/javascript"></script>
                        <script type="text/javascript">
                            var locations = [
                              ['English Caxias do Sul', -29.170967,-51.1887949, 4],
                              ['English Farroupilha', -29.224071, -51.342552, 4]
                            ];

                            var map = new google.maps.Map(document.getElementById('map'), {
                              zoom: 12,
                              center: new google.maps.LatLng(-29.201666, -51.273508),
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                            });

                            var infowindow = new google.maps.InfoWindow();

                            var marker, i;

                            for (i = 0; i < locations.length; i++) {
                              marker = new google.maps.Marker({
                                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                icon: 'http://www.englishlabs.com.br/novo/front/images/favicon.png',
                                map: map
                              });

                              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                  infowindow.setContent(locations[i][0]);
                                  infowindow.open(map, marker);
                                }
                              })(marker, i));
                            }
<<<<<<< HEAD
                        </script>

=======
                        </script>                        
>>>>>>> 79bfab97ee1f111b4fcf93dcf055d0e1e4f57d19
                    </div>
                </div>
                <!-- /Google Map -->
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>