<?php $__env->startSection('title', 'AdminLTE'); ?>

<?php $__env->startSection('content_header'); ?>
<h1>Staff Textos</h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">
                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="<?php echo e(empty($item->id) ? route('admin.stafftext.store') : route('admin.stafftext.store.{id}', array($item->id))); ?>" class="form-table-list" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <!-- text input -->
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" placeholder="Título" name="title" value="<?php echo e(empty($item->title) ? old('title') : $item->title); ?>">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="desc"><?php echo e(empty($item->desc) ? old('desc') : $item->desc); ?></textarea>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
<?php echo $__env->yieldContent('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>