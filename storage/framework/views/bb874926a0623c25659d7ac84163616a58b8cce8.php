<?php $__env->startSection('title', 'AdminLTE'); ?>

<?php $__env->startSection('content_header'); ?>
<h1>Processos > Editar</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Editar Campos</h3>
            </div>
            <form action="<?php echo e(route('admin.processdesc.update.{id}', array($item->id))); ?>" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="box-body">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="image">Imagem:</label>
                            <div class="upload">
                                <div class="btn btn-primary">Atualizar Imagem</div>
                                <input type="file" class="input-file" id="image" name="file_image" /><br>
                                <?php if(!empty($item->id)): ?>
                                (<a href="<?php echo e(asset('_files/processos/' . $item->image)); ?>" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
                                <?php endif; ?>
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="desc"><?php echo e(empty($item->desc) ? old('desc') : $item->desc); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success btn-send pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/css/main.css')); ?>">
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
<?php echo $__env->yieldContent('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>