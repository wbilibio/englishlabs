<!doctype html>
<html lang="en">
    <head>
        <title>English Labs</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--header icon CSS -->
        <link rel="icon" href="<?php echo e(asset('front/images/favicon.png')); ?>">
        <!-- style CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('front/css/style.css')); ?>">
        <!-- responsive CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('front/css/responsive.css')); ?>">
        <!-- maps CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('front/ammap/ammap.css')); ?>" type="text/css">
        <script>

            window.onbeforeunload = function() {
                return alert(1);
            };
        </script>
    </head>

    <body class="sticky-header sticky-footer">
        <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body>
</html>