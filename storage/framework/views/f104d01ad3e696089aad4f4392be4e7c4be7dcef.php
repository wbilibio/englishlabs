<?php $__env->startSection('content_header'); ?>
<h1>Pelo Mundo</h1>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Editar Campos</h3>
            </div>
            <form action="<?php echo e(route('admin.pelomundo.update.{id}', array($item->id))); ?>" method="POST" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="box-body">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" class="form-control" placeholder="Nome" name="title" value="<?php echo e(empty($item->title) ? old('title') : $item->title); ?>">
                        </div>
                        <div class="form-group">
                            <label>Latitude</label>
                            <input type="text" class="form-control" placeholder="Latitude" name="latitude" value="<?php echo e(empty($item->latitude) ? old('latitude') : $item->latitude); ?>">
                        </div>
                        <div class="form-group">
                            <label>Longitude</label>
                            <input type="text" class="form-control" placeholder="Longitude" name="longitude" value="<?php echo e(empty($item->longitude) ? old('longitude') : $item->longitude); ?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    <button class="btn btn-success btn-send pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('vendor/adminlte/css/main.css')); ?>">
<?php $__env->stopSection(); ?>

<?php echo $__env->make('adminlte::page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>