<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventdescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventdesc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', 255)->nullable();
	        $table->string('data', 255)->nullable();
            $table->longText('desc')->nullable();
            $table->longText('text')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventdesc');
    }
}
