<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (\App\User::count() > 0) {
            return;
        }
        DB::table('users')->insert([
            'name' => 'Allan Christian',
            'email' => 'allan.cnfx@gmail.com',
            'password' => bcrypt('master1234'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'name' => 'Atendimento',
            'email' => 'contato@englishlabs.com.br',
            'password' => bcrypt('atendimento1234'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
